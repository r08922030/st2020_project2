const puppeteer = require('puppeteer');
const url = 'http://localhost:3000'
const openBrowser = true; 

// 1 - example
test('[Content] Login page title should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    let title = await page.$eval('body > div > form > div:nth-child(1) > h3', (content) => content.innerHTML);
    expect(title).toBe('Login');
    await browser.close();
})


// 2 
test('[Content] Login page join button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    let button = await page.$eval('body > div > form > div:nth-child(4) > button', (content) => content.innerHTML);
    expect(button).toBe('Join');
    await browser.close();
})

// 3 - example
test('[Screenshot] Login page', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.screenshot({path: 'test/screenshots/login.png'});
    await browser.close();
})

// 4
test('[Screenshot] Welcome message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 1000}); 
    await page.screenshot({path: 'test/screenshots/welcome.png'});
    await browser.close();
})

// 5
test('[Screenshot] Error message when you login without user and room name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.keyboard.press('Enter', {delay: 1000}); 
    await page.screenshot({path: 'test/screenshots/error.png'});
    await browser.close();
})

// 6
test('[Content] Welcome message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 
    await page.waitForSelector('#messages > li > .message__body > p');
    let message = await page.evaluate(() => {
        return document.querySelector('#messages > li > .message__body > p').innerHTML;
    });
    expect(message).toBe('Hi John, Welcome to the chat app');

    await browser.close();

})

// 7 - example
test('[Behavior] Type user name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', 100);
    let userName = await page.evaluate(() => {
        return document.querySelector('body > div > form > div:nth-child(2) > input[type=text]').value;
    });
    expect(userName).toBe('John');

    await browser.close();
})

// 8
test('[Behavior] Type room name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', 100);
    let roomName = await page.evaluate(() => {
        return document.querySelector('body > div > form > div:nth-child(3) > input[type=text]').value;
    });
    expect(roomName).toBe('R1');

    await browser.close();

})

// 9 - example
test('[Behavior] Login', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 

    await page.waitForSelector('#users > ol > li');    
    let member = await page.evaluate(() => {
        return document.querySelector('#users > ol > li').innerHTML;
    });

    expect(member).toBe('John');
    
    await browser.close();
})

// 10
test('[Behavior] Login 2 users', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page1 = await browser.newPage();
    //const browser2 = await puppeteer.launch({ headless: !openBrowser });
    const page2 = await browser.newPage();
    
    await page1.goto(url);

    await page1.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page1.type('body > div > form > div:nth-child(3) > input[type=text]', 'R2', {delay: 100});
    await page1.keyboard.press('Enter', {delay: 100}); 

    await page2.goto(url);

    await page2.type('body > div > form > div:nth-child(2) > input[type=text]', 'Alice', {delay: 100});
    await page2.type('body > div > form > div:nth-child(3) > input[type=text]', 'R2', {delay: 100});
    await page2.keyboard.press('Enter', {delay: 100}); 


    await page2.waitForSelector('#users > ol > li');    
    let member = await page2.evaluate(() => 
        Array.from(document.querySelectorAll('#users > ol > li'), e => e.textContent)
    );
    expect(member[0]).toBe('John');
    expect(member[1]).toBe('Alice');
    
    await browser.close();
})

// 11
test('[Content] The "Send" button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 
    let button = await page.$eval('#message-form > button', (content) => content.innerHTML);
    expect(button).toBe('Send');
    await browser.close();

})

// 12
test('[Behavior] Send a message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser});
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 
    const animationTime = 3000;
    await new Promise((resolve) => setTimeout(resolve, animationTime)); // wait for animation to finish
    await page.waitForSelector('#message-form > input[type=text]'); 
    await page.type('#message-form > input[type=text]', 'Hello', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 

    await page.waitForSelector('#messages > li:nth-child(2) > .message__body > p');    

    let msg = await page.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(2) > .message__body > p').innerHTML;
    });
    expect(msg).toBe('Hello');
    await browser.close();

})

// 13
test('[Behavior] John says "Hi" and Mike says "Hello"', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser});
    const page1 = await browser.newPage();
    const animationTime = 2000;

    await page1.goto(url);
    await page1.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page1.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page1.keyboard.press('Enter', {delay: 100}); 
    await new Promise((resolve) => setTimeout(resolve, animationTime)); // wait for animation to finish
    await page1.waitForSelector('#message-form > input[type=text]'); 
    await page1.type('#message-form > input[type=text]', 'Hi', {delay: 100});
    await page1.keyboard.press('Enter', {delay: 100}); 

    const page2 = await browser.newPage();
    await page2.goto(url);
    await page2.type('body > div > form > div:nth-child(2) > input[type=text]', 'Mike', {delay: 100});
    await page2.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page2.keyboard.press('Enter', {delay: 100}); 
    await new Promise((resolve) => setTimeout(resolve, animationTime)); // wait for animation to finish
    await page2.waitForSelector('#message-form > input[type=text]'); 
    await page2.type('#message-form > input[type=text]', 'Hello', {delay: 100});
    await page2.keyboard.press('Enter', {delay: 100}); 

    await page1.waitForSelector('#messages > li:nth-child(4) > .message__body > p');
    let msg1 = await page1.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(2) > .message__title > h4').innerHTML;
    });
    let msg2 = await page1.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(2) > .message__body > p').innerHTML;
    });
    let msg3 = await page1.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(4) > .message__title > h4').innerHTML;
    });
    let msg4 = await page1.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(4) > .message__body > p').innerHTML;
    });
    expect(msg1).toBe('John');
    expect(msg2).toBe('Hi');
    expect(msg3).toBe('Mike');
    expect(msg4).toBe('Hello'); 
    await browser.close();
})

// 14
test('[Content] The "Send location" button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 
    let button = await page.$eval('#send-location', (content) => content.innerHTML);
    expect(button).toBe('Send location');
    await browser.close();

})

// 15
test('[Behavior] Send a location message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser , ignoreHTTPSErrors: true});
    const page = await browser.newPage();
    
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 
    const animationTime = 2000;
    await new Promise((resolve) => setTimeout(resolve, animationTime)); // wait for animation to finish

    const context = browser.defaultBrowserContext();
    await context.overridePermissions('https://localhost:3000', ['geolocation']);

    await page.waitForSelector('#send-location'); 
    await page.click('#send-location', {delay: 100});
    await browser.close();
})
